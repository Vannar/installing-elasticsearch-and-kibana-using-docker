## Prerequisites: 
1.) Docker must be installed on the Ubuntu server.

2.) Port 5601 (Kibana) must be exposed to the outside world.

3.) Port 9200 (for elasticsearch) must be exposed to the outside world.


# Run the following commands to run Kibana and elasticsearch using Docker.

First, create a Docker network so that both Kibana and Elasticsearch run on the same network.

-> docker network create elastic

Now run the elasticsearch container by running following commands 

-> sysctl -w vm.max_map_count=262144

-> docker pull docker.elastic.co/elasticsearch/elasticsearch:8.6.2

-> docker run --name es-node01 --net elastic -p 9200:9200 -p 9300:9300 -t docker.elastic.co/elasticsearch/elasticsearch:8.6.2

After running the following commands 

When you start Elasticsearch for the first time, the following security configuration occurs automatically:

Certificates and keys are generated for the transport and HTTP layers.
The Transport Layer Security (TLS) configuration settings are written to elasticsearch.yml.
A password is generated for the elastic user.
An enrollment token is generated for Kibana.

Copy the generated password and enrollment token and save them in a secure location. These values are shown only when you start Elasticsearch for the first time. You’ll use these to enroll Kibana with your Elasticsearch cluster and log in.

## You can refer to the "elastic.png" file.

## In a new terminal session, start Kibana and connect it to your Elasticsearch container:

-> docker pull docker.elastic.co/kibana/kibana:8.6.2

-> docker run --name kib-01 --net elastic -p 5601:5601 docker.elastic.co/kibana/kibana:8.6.2

## You can refer to the "kibana.png" file.

after entering the enrollment token , kibana will ask for verirfication code , verification code will be available at the terminal on which kibana is running

After verification you will be directed to the login page
Username: elastic
Password: <A password is generated for kibana when we run the elastic container>

Finally, the Kibana dashboard will be displayed!

## you can refer to the "kibana_dashboard.png"





